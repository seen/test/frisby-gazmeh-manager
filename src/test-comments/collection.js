/*
 * The MIT License (MIT)
 * Copyright (c) 2016 phoenix-scholars http://dpq.co.ir
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
const config = require('../config.js');
const schema = require('../schemas-gazmeh-management.js');
const seen = require('seen-e2et-frisby');

var testData = [ {
	mime_type : 'text/plain',
	text : 'Example 1',
}, {
	mime_type : 'text/plain',
	text : 'Exmaple 2',
}, {
	text : 'Exmaple 2',
} ];

var models = [ {} ];

/**
 * Init test data and modules
 */
function initTestData(test){
	for (var i = 0; i < testData.length; i++) {
		testData[i].test = test.id;
		models[0].id = test.id;
	}
	return test;
}

// 1- Create a test and project
beforeAll(function(done) {
	schema.createTestDataProject()
	.then(schema.createTestDataTest)
	.then(initTestData)
	.done(done);
});

// 2 generate tests
seen.builder.collection.fetchProtectedTests({
	resource : config.endpoint + '/api/v2/gazmeh/test-comments',
	jsonTypes : schema.testComment,
	testData : testData
});



//3 generate bulky tests
seen.builder.collection.bulkyProtectedTests({
	resource : config.endpoint + '/api/v2/gazmeh/test-comments',
	jsonTypes : schema.testComment,
	testData : testData,
});
