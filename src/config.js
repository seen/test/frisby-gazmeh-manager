/*
 * The MIT License (MIT)
 * Copyright (c) 2016 phoenix-scholars http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
exports.endpoint = 'http://localhost:8083';
exports.version = '2';
if (process.env['endpoint']) {
	exports.endpoint = process.env['endpoint'];
}

const admin = {
		login : 'admin',
		password : 'admin'
};

const fakeAdminPass = {
		login : 'admin1',
		password : 'admin'
};
//data
exports.admin = admin;
exports.fakeAdminPass = fakeAdminPass;

/*
 * To support old tests
 */
//functions
//var seen = require('seen-e2et-frisby');
//exports.createGetCollectionTest = seen.builder.collection.fetchTest;
//exports.basicProtectedCollectionTests = seen.builder.collection.fetchProtectedTests;
//exports.bulkyProtectedCollectionTests = seen.builder.collection.bulkyProtectedTests;
//
//exports.basicProtectedBinaryTests = seen.builder.binary.protectedBinaryTest;

