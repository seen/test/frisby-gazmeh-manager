/*
 * The MIT License (MIT)
 * Copyright (c) 2016 phoenix-scholars http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
const Joi = require('joi');
const frisby = require('frisby');
const qs = require('qs');
const config = require('./config.js');


exports.project = {
		'id' : Joi.number().required(),
};

exports.requirement = {
		'id' : Joi.number().required(),
		'type' : Joi.string(),
		'title' : Joi.string().allow(null),
		'description' : Joi.string().allow(null),
		'project' : Joi.number().integer().positive().required()
};

exports.test = {
		'id' : Joi.number().required(),
		'type' : Joi.string().allow(null),
		'title' : Joi.string().allow(null),
		'state' : Joi.string().allow(null),
};
exports.testRisk = {
		'id' : Joi.number().required(),
		'title' : Joi.string(),
		'description' : Joi.string(),
		'probability' : Joi.number(),
		'effect' : Joi.string(),
		'test' : Joi.number(),
};
exports.testAttachment = {
		'id' : Joi.number().required(),
}

exports.activity = {
		'id' : Joi.number().required(),
		'title' : Joi.string(),
		'type' : Joi.string(),
		'description' : Joi.string().allow(null),
		'assign' : Joi.number(),
		'start_dtime' : Joi.string().allow(null),
		'end_dtime' : Joi.string().allow(null)
};
exports.activityComment = {
		'id' : Joi.number().required(),
		'mime_type' : Joi.string(),
		'text' : Joi.string(),
		'user' : Joi.number(),
		'activity' : Joi.number(),
		'creation_dtime' : Joi.string().required()
};
exports.activityLog = {
		'id' : Joi.number().required(),
		'title' : Joi.string(),
		'description' : Joi.string(),
		'duration' : Joi.string(),
		'creation_dtime' : Joi.string().required()
};

exports.vu = {
		'id' : Joi.number().required(),
		'title' : Joi.string(),
		'description' : Joi.string(),
		'test' : Joi.number().required(),
};

exports.scenario = {
		id : Joi.number().required(),
};

exports.testAcceptanceCriteria = {
		id : Joi.number().required(),
		title :  Joi.string(),
		description :  Joi.string(),
		metric:  Joi.string(),
		bound:  Joi.number(),
		bound_type:  Joi.string(),
		severity:  Joi.string(),
		duration:  Joi.number(),
		duration_type:  Joi.string(),
		test : Joi.number().required(),
}

exports.testRun = {
		id : Joi.number().required(),
		title :  Joi.string().allow(null),
		description :  Joi.string().allow(null),
}

/*****************************************************************************
 *                               Utilitis                                    *
 *                                                                           *
 *                                                                           *
 *****************************************************************************/



var headers = {
		'Authorization' : 'Basic ' + Buffer.from(config.admin.login + ":" + config.admin.password).toString('base64'),
		'Content-Type' : 'application/x-www-form-urlencoded'
};

/**
 * Create a random project for test
 */
function createTestDataProject() {
	return frisby
	.post(config.endpoint + '/api/v2/gazmeh/projects', {
		body : qs.stringify({
			title : 'Test project',
			state : 'stopped',

		}),
		headers : headers
	})
	.expect('successResponses')
	.expect('jsonTypes', exports.project)
	.then(function(res) {
		return res.json;
	});
}

/**
 * Crate a random test for test
 */
function createTestDataTest(project) {
	return frisby
	.post(config.endpoint + '/api/v2/gazmeh/tests', {
		body : qs.stringify({
			title : 'Test test',
			state : 'stopped',
			type : 'functional',
			project : project.id
		}),
		headers : headers
	})
	.expect('successResponses')
	.expect('jsonTypes', exports.test)
	.then(function(res) {
		return res.json;
	});
}

/**
 * Crate a random virtual user for test
 */
function createTestDataVirtualUser(test) {
	return frisby
	.post(config.endpoint + '/api/v2/gazmeh/virtual-users', {
		body : qs.stringify({
			title : 'when_CallApiV2Gazmeh-VU#1',
			description : 'stopped',
			enable : true,
			test : test.id,
			type: 'test'
		}),
		headers : headers
	})
	.expect('successResponses')
	.expect('jsonTypes', exports.vu)
	.then(function(res) {
		return res.json;
	});
}

/**
 * Crate a random test attachment
 */
function createTestDataAttachment(test) {
	return frisby
	.post(config.endpoint + '/api/v2/gazmeh/attachments', {
		body : qs.stringify({
			title : 'when_CallApiV2Gazmeh-attachment#1',
			test : test.id
		}),
		headers : headers
	})
	.expect('successResponses')
	.expect('jsonTypes', exports.testAttachment)
	.then(function(res) {
		return res.json;
	});
}


/**
 * Crate a random test attachment
 */
function createTestDataActivity(test) {
	return frisby
	.post(config.endpoint + '/api/v2/gazmeh/activities', {
		body : qs.stringify({
			title : 'test data-activity',
			description: 'Random test activity',
			test : test.id,
			type: 'script',
			assign: 1
		}),
		headers : headers
	})
	.expect('successResponses')
	.expect('jsonTypes', exports.activity)
	.then(function(res) {
		return res.json;
	});
}

function createTestDataScenario(vu) {
	return frisby
	.post(config.endpoint + '/api/v2/gazmeh/scenarios', {
		body : qs.stringify({
			title : 'when_CallApiV2Gazmeh-VU#1',
			description : 'stopped',
			enable : true,
			virtual_user : vu.id,
			type: 'test'
		}),
		headers : headers
	})
	.expect('successResponses')
	.expect('jsonTypes', exports.scenario)
	.then(function(res) {
		return res.json;
	});
}

function createTestDatarRquirement(project) {
	return frisby
	.post(config.endpoint + '/api/v2/gazmeh/requirements', {
		body : qs.stringify({
			title : 'when_CallApiV2Gazmeh-Test#1',
			description : 'stopped',
			type : 'functional',
			project: project.id
		}),
		headers : headers
	})
	.expect('successResponses')
	.expect('jsonTypes', exports.requirement)
	.then(function(res) {
		return res.json;
	});
}

exports.createTestDataScenario = createTestDataScenario;
exports.createTestDataProject = createTestDataProject;
exports.createTestDataTest = createTestDataTest;
exports.createTestDataVirtualUser = createTestDataVirtualUser;
exports.createTestDataAttachment = createTestDataAttachment;
exports.createTestDataActivity = createTestDataActivity;
exports.createTestDatarRquirement = createTestDatarRquirement;
