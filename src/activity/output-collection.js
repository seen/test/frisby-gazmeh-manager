/*
 * The MIT License (MIT)
 * Copyright (c) 2016 phoenix-scholars http://dpq.co.ir
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
const config = require('../config.js');
const schema = require('../schemas-gazmeh-management.js');
const seen = require('seen-e2et-frisby');

var testData = [{
	title: 'text/plain',
	description: 'Example 1',
},{
	title: 'text/plain',
	description: 'Exmaple 2',
},{
	title: 'Exmaple 2',
}];

var models = [ {
	id : null
} ];

function initTestData(activity) {
	models[0].id = activity.id;
}

//1- Create a test and project
beforeAll(function(done) {
	schema.createTestDataProject()
		.then(schema.createTestDataTest)
		.then(schema.createTestDataActivity)
		.then(initTestData)
		.done(done);
});

seen.builder.collection.fetchProtectedTests({
	resource : config.endpoint + '/api/v2/gazmeh/activities/{id}/outputs',
	models : models,
	testData: testData
});