/*
 * The MIT License (MIT)
 * Copyright (c) 2016 phoenix-scholars http://dpq.co.ir
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
const config = require('../config.js');
const schema = require('../schemas-gazmeh-management.js');
const seen = require('seen-e2et-frisby');

var testData = [{
	title: 'when_CallApiV2Gazmeh-Test#1',
	state: 'stopped',
	state: 'ready',
	type: 'script',
}];


var models = [ {
	id : null
} ];


function initTestData(test) {
	models[0].id = test.id;
	for(var i = 0; i < testData.length; i++){
		testData[i].test = test.id;
	}
//	return test;
}

//1- Create a test and project
beforeAll(function(done) {
	schema.createTestDataProject()
		.then(schema.createTestDataTest)
		.then(initTestData)
		.done(done);
});

seen.builder.collection.fetchProtectedTests({
	resource : config.endpoint + '/api/v2/gazmeh/activities',
	models : models,
	testData: testData
});